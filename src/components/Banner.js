import React from 'react';

/*react bootstrap*/
import {Container, Row, Col, Jumbotron, Button} from 'react-bootstrap';

export default function Banner() {
	return(
		<Container fluid>
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-5">
						<h1>Zuitt Course Booking</h1>
						<p>Oppurtunities for everyone, everywhere.</p>
						<Button variant="primary">Enroll</Button>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}