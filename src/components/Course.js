import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';


import {Card, Button} from 'react-bootstrap';

export default function Course ({courseProp}) {
	/*console.log(courseProp)*/

	const {_id, name, description, price} = courseProp

	//state
/*	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(10);
	const [isDisabled, setIsDisabled] = useState(false);


function enroll() {
	if (seat > 0) {
		setCount(count + 1)
		setSeat(seat - 1)
	} 
}

useEffect(()=>{
	if (seat === 0) {
		setIsDisabled(true)
	}
}, [seat]);
*/
	return( 
		<Card className="mx-4 mb-5">
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Text>{description}</Card.Text>
		    <Card.Text>Php {price}</Card.Text>
		    {/*<h5>Enrollees</h5>
		    <p>{count} Enrollees</p>
		    <p>{seat} Seats</p>
		    <Button variant="primary" onClick={enroll} disabled={isDisabled}>Enroll</Button>*/}
		  	<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>
	)
}

Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}


/*
DIFFERENT APPROACH-------------------------------

import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';


import {Card, Button} from 'react-bootstrap';

export default function Course(props){
	console.log(props)
	let course = props.course

	//[state, setState] = useState()
	// const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(10);
	const [isDisabled, setIsDisabled] = useState(false);

	useEffect( () => {
		if(seat === 0){
			setIsDisabled(true)
		}
	}, [seat]);


	return(
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{course.name}</Card.Title>
				<h5>Description</h5>
				<p>{course.description}</p>
				<h5>Price:</h5>
				<p>{course.price}</p>
				<h5>Seats</h5>
				<p>{seat} Seats</p>
		    	<Button variant="primary" onClick={ 
		    		() => { setSeat( seat - 1)}
		    	} disabled={isDisabled}>Enroll</Button>
			</Card.Body>
		</Card>
	)
}


Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

*/ 