import React from 'react';

import {Container, Row, Col, Card, Button} from 'react-bootstrap';


export default function CourseCard () {
  return (
  	<Container fluid>
  		<Row>
  			<Col xs={12} md={12}>
  				<Card className="mx-4 mb-5">
  				  <Card.Body>
  				    <Card.Title>Sample Course</Card.Title>
  				    <Card.Text>Description:</Card.Text>
  				    <Card.Text>This is a wider card with supporting text below as a natural lead-in to
  				        additional content. This card has even longer content than the first to
  				        show that equal height action.</Card.Text>
  				    <Card.Text>Price:</Card.Text>
  				    <Card.Text>PhP: 40,000</Card.Text>
  				    <Button variant="primary">Enroll</Button>
  				  </Card.Body>
  				</Card>
  			</Col>
  		</Row>
  	</Container>
  )
}

