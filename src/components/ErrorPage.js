import React from 'react';
import {Link} from 'react-router-dom'
/*react bootstrap*/
import {Container, Row, Col, Jumbotron, Button} from 'react-bootstrap';

export default function ErrorPage() {
	return(
		<Container fluid>
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-5">
						<h1>Error 404</h1>
						<p>Page not found.</p>
						<Button variant="primary" as={Link} to="/">Back</Button>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}