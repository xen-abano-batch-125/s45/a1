import React from 'react';

import {Container, Row, Col, Card, Button} from 'react-bootstrap';


export default function Highlights () {
  return (
  	<Container fluid className="my-3">
        <Row>
          <Col xs={12} md={4}>
            <Card className="mx-3 my-3">
              <Card.Body>
                  <Card.Title>Learn from Home</Card.Title>
                  <Card.Text>
                  Some quick example text to build on the card title and make up the bulk of
                  the card's content.
                  </Card.Text>
                  <Button variant="primary">Go somewhere</Button>
             </Card.Body>
            </Card>
          </Col>
          <Col xs={12} md={4}>
            <Card className="mx-3 my-3">
              <Card.Body>
                  <Card.Title>Study Now Pay Later</Card.Title>
                  <Card.Text>
                  Some quick example text to build on the card title and make up the bulk of
                  the card's content.
                  </Card.Text>
                  <Button variant="primary">Go somewhere</Button>
             </Card.Body>
            </Card>
          </Col>
          <Col xs={12} md={4}>
            <Card className="mx-3 my-3">
              <Card.Body>
                  <Card.Title>Be part of the Community</Card.Title>
                  <Card.Text>
                  Some quick example text to build on the card title and make up the bulk of
                  the card's content.
                  </Card.Text>
                  <Button variant="primary">Go somewhere</Button>
             </Card.Body>
            </Card>
          </Col>
        </Row>  
    </Container>
  )
}

