import React from 'react';
import ReactDOM from 'react-dom';

/*add this from react bootstrap*/
import 'bootstrap/dist/css/bootstrap.min.css';

/*Components*/
import App from './App'


ReactDOM.render(
  <App/>
  ,
  document.getElementById('root')
);

