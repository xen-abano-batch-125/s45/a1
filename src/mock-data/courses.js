export default [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aut reiciendis asperiores animi omnis soluta quaerat esse perspiciatis natus voluptatibus! Optio placeat inventore voluptatum at laboriosam consequatur fugit eius iure, perferendis.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aut reiciendis asperiores animi omnis soluta quaerat esse perspiciatis natus voluptatibus! Optio placeat inventore voluptatum at laboriosam consequatur fugit eius iure, perferendis.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aut reiciendis asperiores animi omnis soluta quaerat esse perspiciatis natus voluptatibus! Optio placeat inventore voluptatum at laboriosam consequatur fugit eius iure, perferendis.",
		price: 55000,
		onOffer: true
	}
]
