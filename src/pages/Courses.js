import React, {useState, useEffect, useContext} from 'react';
import {Container} from 'react-bootstrap';
/*import Course from './../components/Course';*/
import AdminView from './../components/AdminView';
import UserView from './../components/UserView';
import UserContext from './../UserContext';

export default function Courses () {

	const [courses, setCourses] = useState([]);
	const {user} = useContext(UserContext);

	const fetchData = () => {

		let token = localStorage.getItem("token");

		fetch('https://frozen-woodland-41600.herokuapp.com/api/courses/all', {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then( result => result.json())
		.then( result => {
			/*console.log(result)*/
			setCourses(result)
		})
	}

	useEffect(()=> {
		fetchData()
	}, [])

		return (
			<Container fluid>
				{ (user.isAdmin === true) ?
				<AdminView courseData = {courses} fetchData={fetchData}/> //courseData is a property name
				:
				<UserView courseData = {courses}/ >
				}
			</Container>
		)
	}