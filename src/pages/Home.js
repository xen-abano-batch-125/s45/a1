import React from 'react';


/*components*/
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

export default function Home() {

	return(
		<div>
			<Banner/>
			<Highlights/>
		</div>
	)
}