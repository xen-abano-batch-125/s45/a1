import React, {useContext, useEffect, useState} from 'react';
import {Container, Card, Button} from 'react-bootstrap'
import UserContext from './../UserContext'
import {Link, useParams, useHistory} from 'react-router-dom'
import Swal from 'sweetalert2';

export default function SpecificCourse() {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const {user} = useContext(UserContext)

	const {courseId} = useParams();

	let history = useHistory();

	let token = localStorage.getItem('token')

	useEffect(()=>{
		fetch(`https://frozen-woodland-41600.herokuapp.com/api/courses/${courseId}`, {
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [])

	const enroll = () => {
		fetch(`https://frozen-woodland-41600.herokuapp.com/api/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			if (result === true) {
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Successfully Enrolled."
				})
				history.push('/courses');

			} else {

				Swal.fire({
					title: "Failed",
					icon: "error",
					"text": "Please try again."
				})
			}
		})
	}

	return(
		<Container>
			<Card>
				<Card.Header>
					<h4>
						{name}
					</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>
						{description}
					</Card.Text>
					<h6>
						Price: Php {price}
					</h6>
				</Card.Body>
				<Card.Footer>
					{
						(user.id !== null) ?
								<Button variant="info" onClick={()=>enroll()}>Enroll</Button>
							:
								<Link className="btn btn-danger" to="/login">Login to enroll</Link>
					}
				</Card.Footer>
			</Card>
		</Container>
	)
}